from django.apps import AppConfig


class BillgenerationConfig(AppConfig):
    name = 'billgeneration'
