# Generated by Django 3.0.8 on 2020-12-22 13:41

from django.db import migrations, models
import djongo.models.fields
import finqs.models


class Migration(migrations.Migration):

    dependencies = [
        ('finqs', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProcessHistorical',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('processname', models.CharField(max_length=300)),
                ('output', djongo.models.fields.ArrayField(model_container=finqs.models.OutputArray)),
            ],
        ),
    ]
