from django.shortcuts import render
from .models import *
from .serializers import *
from rest_framework import generics, mixins
import requests
import uuid
import time
from django.http.response import JsonResponse
from rest_framework.parsers import JSONParser
from rest_framework.decorators import api_view
from rest_framework.pagination import PageNumberPagination
import subprocess
import os
from pytz import timezone 
from datetime import datetime
import json
import io
from rest_framework.response import Response
from IPython.nbformat import current
import redis
import uuid
from IPython import get_ipython
import sys
from io import StringIO
import contextlib
from .suballlog import *
from .logsublogin import *
from .externalurls import *

# from .getstatus import *
redishost=exredishost
password=expass


class StandardResultsSetPagination(PageNumberPagination):
    page_size = 10
    page_size_query_param = 'page_size'
    max_page_size = 10


class BorrowerApi(generics.ListAPIView):
    resource_name = 'borrowers'
    serializer_class = BorrowerSerializer

    def get_queryset(self):
        return Borrower.objects.all()
            
class BorrowerDetailApi(generics.RetrieveUpdateDestroyAPIView):
    resource_name = 'borrowers'
    lookup_field = 'id'
    serializer_class = BorrowerSerializer

    def get_queryset(self):
        return Borrower.objects.all()

class ProcessApi(mixins.CreateModelMixin, generics.ListAPIView):
    resource_name = 'processes'
    serializer_class = ProcessSerialzer

    def get_queryset(self):
        return Process.objects.all()

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwarg)


class Authentication(generics.ListAPIView):
    resource_name = 'logauths'
    serializer_class = UserSerializer

    def get_queryset(self):
        user = self.request.GET['query[value1]']
        password=self.request.GET['query[value]']
        return User.objects.filter(loginid=user,password=password)


class NextAllowableCrudAction(generics.ListAPIView):
    resource_name = 'nextcrudactions'
    serializer_class = CrudEdgeSerializer
    
    def get_queryset(self):
        data=self.request.GET  
        array = data['status'].split(',')
        return CrudEdge.objects.filter(role=data['role'],status__in=array,objname=data['objname'])



class NextAllowableActions(generics.ListAPIView):
    resource_name = 'nextallowableactions'
    serializer_class = EdgeSerializer

    def get_queryset(self):
        data=self.request.GET
        arrayObjName = data['objectname'].split(',')
        array = data['endstatus'].split(',')
        return Edge.objects.filter(objectname__in=arrayObjName,startstatus=data['startstatus'],endstatus__in=array,role=data['role'])


class GetNotEditableFields(generics.ListAPIView):
    resource_name = 'bofieldseditnoaccesses'
    serializer_class = BoFieldEditNoAccessSerializer
    def get_queryset(self):
        data=self.request.GET
        return BoFieldEditNoAccess.objects.filter(user=data['user'],role=data['role'],businessobject=data['businessobject'])

class GetNotViewableFields(generics.ListAPIView):
    resource_name = 'bofieldsviewnoaccesses'
    serializer_class = BoFieldViewNoAccessSerializer
    def get_queryset(self):
        data=self.request.GET
        return BoFieldViewNoAccess.objects.filter(user=data['user'],role=data['role'],businessobject=data['businessobject'])


@api_view(['GET'])
def test(request):
    return JsonResponse({'message': 'test'}, status=200)


class ProcessRunning(generics.ListAPIView):
    resource_name = 'processlogs'
    serializer_class = ProcessLogSerializer

    @contextlib.contextmanager
    def stdoutIO(self,stdout=None):
        old = sys.stdout
        if stdout is None:
            stdout = StringIO()
        sys.stdout = stdout
        yield stdout
        sys.stdout = old
        pass
    def decision_msg(self,message,chk):
        fl=True
        id=''
        if message:
            msg=''
            encoding = 'utf-8'
            if(isinstance(message['data'], bytes)==True):
                data=str(message['data'], encoding)
                msg=data.split('-')[1]
                id=data.split('-')[0]
                pass
            else:
                msg=message['data']
                pass
            if(msg == chk):
                fl=False
                pass
            pass
        else:
            fl=True
            pass
        return fl,id
        
    def convert(self,seconds): 
        seconds = seconds % (24 * 3600) 
        hour = seconds // 3600
        seconds %= 3600
        minutes = seconds // 60
        seconds %= 60
        time="%02d:%02d:%02d" % (hour, minutes, seconds) 
        time=time+"-"+str(seconds)  
        return time
    
    def get_queryset(self):
        sch_log = redis.Redis(host=redishost, port=6379, db=0,password=password)
        nowone = datetime.now(timezone("Asia/Dhaka"))
        dtone=nowone.strftime("%d/%m/%y")
        current_time_one = nowone.strftime("%H:%M:%S")
        start = time.time()
        redis_r = redis.Redis(host=redishost, port=6379, db=0,password=password)
        redis_p = redis_r.pubsub()
        redis_p.subscribe('cancelprocess')
        fl=True
        try:
            id = self.request.GET['process']
            cnt=JobRunningStatus.objects.filter(jobid=id).count()
            if(cnt<1):
                cntP=JobRunningStatus.objects.count()
                cntP=cntP+1
                JobRunningStatus.objects.create(id=cntP,jobid=id,status="1")
                pass
            else:
                JobRunningStatus.objects.filter(jobid=id).update(status="1")
                pass
            process=''
            elapse=''
            cnt=JobDuration.objects.filter(jobid=id).count()
            if(cnt<1):
                elapse='00:00:00'
                pass
            else:
                dataJOB=JobDuration.objects.filter(jobid=id)
                sum=0
                for dx in dataJOB:
                    sum=sum+ float(dx.durationsec)
                    pass
                avg=sum/cnt
                newtime = self.convert(round(avg,4))
                elapse = newtime
                pass
            elapsenew=id+'-'+elapse
            sch_log.publish('esttime',elapsenew)
            processdata=ProcessSheduler.objects.filter(id=id).all()
            for p in processdata:
                process=p.process
                pass
            ProcessLog.objects.all().delete()
            data=Process.objects.filter(name=process).all()
            
            for d in data:
                array=[]
                processname=d.filename
                path="notebooks/"+processname+'.ipynb'
                dir=os.path.dirname(__file__)
                filename = os.path.join(dir, path)
                with io.open(filename) as content_file:
                    content = current.read(content_file, 'json')
                    pass
                
                for cell in content.worksheets[0].cells:
                    message = redis_p.get_message()
                    fl,newid=self.decision_msg(message,'cancel')
                    if(newid==id):
                        if(fl==True):
                            dict={}
                            data = cell['input']
                            dict['input']=cell['input']
                            now = datetime.now(timezone("Asia/Dhaka"))
                            dt=now.strftime("%d/%m/%y")
                            current_time = now.strftime("%H:%M:%S")
                            with self.stdoutIO() as s:
                                exec(data)
                                pass
                            text='.........'
                            if s.getvalue():
                                text=s.getvalue().strip()
                                pass
                            texttosend=str(dt) + "-" +str(current_time) + "-" + text + "-" + str(id)
                            sch_log.publish('schedulerlog',texttosend)
                            dict['dt']=dt
                            dict['time']=current_time
                            dict['output']=s.getvalue().strip()
                            array.append(dict)
                            pass
                        else:
                            texttosend=str(dt) + "-" +str(current_time) + "-" + 'The process is canceled'+ "-" + str(id)
                            sch_log.publish('schedulerlog',texttosend)
                            over=str(id)+"-false"
                            sch_log.publish('schedulerover',over)
                            sch_log.publish('setstopjob',over)
                            nowtwo = datetime.now(timezone("Asia/Dhaka"))
                            current_end = nowtwo.strftime("%H:%M:%S")
                            modeltemHistorical=ProcessHistorical.objects.create(jobid=id,dt=dtone,time=current_time_one,endtime=current_end,status="abort",id=str(uuid.uuid4().hex),processname=process,output=array,runid=str(uuid.uuid4().hex))
                            return ProcessLog.objects.all()
                            pass
                        pass
                    else:
                        dict={}
                        data = cell['input']
                        dict['input']=cell['input']
                        now = datetime.now(timezone("Asia/Dhaka"))
                        dt=now.strftime("%d/%m/%y")
                        current_time = now.strftime("%H:%M:%S")
                        with self.stdoutIO() as s:
                            exec(data)
                            pass
                        text='.........'
                        if s.getvalue():
                            text=s.getvalue().strip()
                            pass
                        texttosend=str(dt) + "-" +str(current_time) + "-" + text + "-" + str(id)
                        sch_log.publish('schedulerlog',texttosend)
                        dict['dt']=dt
                        dict['time']=current_time
                        dict['output']=s.getvalue().strip()
                        array.append(dict)
                        pass
                    pass
                over=str(id)+"-false"
                sch_log.publish('schedulerover',over)
                JobRunningStatus.objects.filter(jobid=id).update(status="0")
                end = time.time()
                nowtwo = datetime.now(timezone("Asia/Dhaka"))
                current_end = nowtwo.strftime("%H:%M:%S")
                diff=end-start
                modeltemHistorical=ProcessHistorical.objects.create(id=str(uuid.uuid4().hex),jobid=id,dt=dtone,time=current_time_one,endtime=current_end,status="success",processname=process,output=array,runid=str(uuid.uuid4().hex))
                JobDuration.objects.create(id=cnt+1,jobid=id,startdatetime=start,enddatetime=end,durationsec=diff,jobstatus="success")
                return ProcessLog.objects.all()

        except Exception as e:
            print(e)
            texttosend='Exception occured'
            sch_log.publish('schedulerlog',texttosend)
            sch_log.publish('schedulerover',"false")
            pass
        


class DayofWeekApi(mixins.CreateModelMixin, generics.ListAPIView):
    resource_name = 'dayofweeks'
    serializer_class = DayOfWeekSerializer
    def get_queryset(self):
        return DayOfWeek.objects.all()
        
    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwarg)


class DayApi(mixins.CreateModelMixin, generics.ListAPIView):
    resource_name = 'days'
    serializer_class = DaySerializer
    def get_queryset(self):
        return Day.objects.all()
        
    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwarg)

class SecondApi(mixins.CreateModelMixin, generics.ListAPIView):
    resource_name = 'seconds'
    serializer_class = SecondSerializer
    def get_queryset(self):
        return Second.objects.all()
        
    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwarg)

class ProcesssSchedulerApi(mixins.CreateModelMixin, generics.ListAPIView):
    resource_name = 'processshedulers'
    serializer_class = ProcessShedulerSerializer

    def get_queryset(self):
        return ProcessSheduler.objects.all()

    def post(self, request, *args, **kwargs):
        count=ProcessSheduler.objects.count()
        request.data['id']=count+1
        JobRunningStatus.objects.create(jobid=request.data['id'],id=request.data['id'],status="0")
        
        return self.create(request, *args, **kwargs)


class JobRunningStatusApi(mixins.CreateModelMixin, generics.ListAPIView):
    resource_name = 'jobstatuses'
    serializer_class = JobRunningStatusSerializer
    def get_queryset(self):
        return JobRunningStatus.objects.all()
        
    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwarg)

class JobRunningStatusDetailApi(generics.RetrieveUpdateDestroyAPIView):
    resource_name = 'jobstatuses'
    lookup_field = 'jobid'
    serializer_class = JobRunningStatusSerializer

    def get_queryset(self):
        return JobRunningStatus.objects.all()

class ProcesssSchedulerDetailApi(generics.RetrieveUpdateDestroyAPIView):
    resource_name = 'processshedulers'
    lookup_field = 'id'
    serializer_class = ProcessShedulerSerializer

    def get_queryset(self):
        return ProcessSheduler.objects.all()

class MinuteApi(mixins.CreateModelMixin, generics.ListAPIView):
    resource_name = 'minutes'
    serializer_class = MinuteSerializer
    def get_queryset(self):
        return Minute.objects.all()
        
    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwarg)

class HourApi(mixins.CreateModelMixin, generics.ListAPIView):
    resource_name = 'hours'
    serializer_class = HourSerializer
    def get_queryset(self):
        return Hour.objects.all()

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwarg)


class ProcessLogHistoricalApi(mixins.CreateModelMixin,generics.ListAPIView):
    resource_name = 'joblogs'
    serializer_class = ProcessHistoricalSerializer

    def get_queryset(self):
        return ProcessHistorical.objects.order_by('id').reverse()
    
    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwarg)

class ProcessLogHistoricalDetailApi(generics.RetrieveUpdateDestroyAPIView):
    resource_name = 'joblogs'
    lookup_field = 'id'
    serializer_class = ProcessHistoricalSerializer

    def get_queryset(self):
        return ProcessHistorical.objects.all()