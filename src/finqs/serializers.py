
from rest_framework import serializers 
from .models import *
import jwt
import os
import uuid

class BorrowerSerializer(serializers.ModelSerializer):
    class Meta:
        model=Borrower
        fields='__all__'

class ProcessSerialzer(serializers.ModelSerializer):
    class Meta:
        model=Process
        fields='__all__'

class ProcessLogSerializer(serializers.ModelSerializer):
    class Meta:
        model=ProcessLog
        fields='__all__'

class ProcessHistoricalSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField(source='get_name')
    def get_name(self,obj):
        name=""
        data=ProcessSheduler.objects.filter(id=obj.jobid).all()
        for d in data:
            name=d.jobname
            pass
        return name
    class Meta:
        model=ProcessHistorical
        fields=("jobid","dt","time","processname","output","name","endtime","status","name","runid","id")

class EdgeSerializer(serializers.ModelSerializer):
    class Meta:
        model=Edge
        fields=('id','objectname','nextaction','startstatus','endstatus','role')

class CrudEdgeSerializer(serializers.ModelSerializer):
    class Meta:
        model=CrudEdge
        fields=('id','objname','status','role','crudaction')


class UserSerializer(serializers.ModelSerializer):
    token = serializers.SerializerMethodField(source='get_token')
    role = serializers.SerializerMethodField(source='get_role')

    def get_token(self,obj):

        access_token_payload = {
            'user_id': obj.loginid,
            }
        access_token = str(uuid.uuid4().hex)
        return  access_token
    
    def get_role(self,obj):
        user=UserRole.objects.filter(loginid=obj.loginid)
        role=''
        for u in user:
            role=u.role
            pass
        return role
        
    class Meta:
        model=User
        fields=('id','name','loginid','password','token','role')

class BoFieldEditNoAccessSerializer(serializers.ModelSerializer):
    class Meta:
        model=BoFieldEditNoAccess
        fields=('id','user','role','businessobject','attribute')

class BoFieldViewNoAccessSerializer(serializers.ModelSerializer):
    class Meta:
        model=BoFieldViewNoAccess
        fields=('id','user','role','businessobject','attribute')

class SecondSerializer(serializers.ModelSerializer):
    class Meta:
        model=Second
        fields=('id','second')

class MinuteSerializer(serializers.ModelSerializer):
    class Meta:
        model=Minute
        fields=('id','minute')

class HourSerializer(serializers.ModelSerializer):
    class Meta:
        model=Hour
        fields=('id','hr')

class DayOfWeekSerializer(serializers.ModelSerializer):
    class Meta:
        model=DayOfWeek
        fields=('id','day')

class DaySerializer(serializers.ModelSerializer):
    class Meta:
        model=Day
        fields=('id','day')

class ProcessShedulerSerializer(serializers.ModelSerializer):
    class Meta:
        model=ProcessSheduler
        fields=('id','process','isweekly','ismonthly','isdaily','ishourly','month','monthday','weekday','hour','minute','second','status','dayofweek','jobname')


class JobDurationSerializer(serializers.ModelSerializer):
    class Meta:
        model=JobDuration
        fields="__all__"

class JobRunningStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model=JobRunningStatus
        fields="__all__"