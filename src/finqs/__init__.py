from django.db import migrations, models


class Migration(migrations.Migration):
    initial = True

    dependencies = [
    ]
    operations = [
        migrations.CreateModel(
            name='processlog',
            fields=[
                ('id', models.AutoField(auto_created=True,
                                        primary_key=True, serialize=False, verbose_name='ID')),
            ],
        ),
        migrations.CreateModel(
            name='processhistorical',
            fields=[
                ('id', models.AutoField(auto_created=True,
                                        primary_key=True, serialize=False, verbose_name='ID')),
            ],
        )
    ]
