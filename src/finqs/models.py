from django.db import models
import time
import random
from djongo import models as modeldb

class LoginLog(models.Model):
    class Meta:
        db_table="login_log"
        pass
    datetimestamp=models.CharField(max_length=300)
    dt=models.CharField(max_length=300)
    time=models.CharField(max_length=300)
    userid=models.CharField(max_length=300)
    username=models.CharField(max_length=300)
    lat=models.CharField(max_length=300)
    lng=models.CharField(max_length=300)
    ip=models.CharField(max_length=300)
    inputuserid=models.CharField(max_length=300)
    inputpassword=models.CharField(max_length=300)
    userrole=models.CharField(max_length=300)
    activitytype=models.CharField(max_length=300)
    status=models.CharField(max_length=300)
    statuscode=models.CharField(max_length=300)
    variablebefore=models.CharField(max_length=1000)
    variableafter=models.CharField(max_length=1000)
    comment=models.CharField(max_length=1000)
    reason=models.CharField(max_length=1000)
    reasoncode=models.CharField(max_length=1000)



class AllLog(models.Model):
    class Meta:
        db_table="all_log"
        pass
    datetimestamp=models.CharField(max_length=300)
    dt=models.CharField(max_length=300)
    time=models.CharField(max_length=300)
    userid=models.CharField(max_length=300)
    username=models.CharField(max_length=300)
    userrole=models.CharField(max_length=300)
    activitycat=models.CharField(max_length=300)
    activitysubcat=models.CharField(max_length=300)
    status=models.CharField(max_length=300)
    statuscode=models.CharField(max_length=300)
    variablebefore=models.CharField(max_length=1000)
    variableafter=models.CharField(max_length=1000)
    comment=models.CharField(max_length=1000)
    reason=models.CharField(max_length=1000)
    reasoncode=models.CharField(max_length=1000)

class User(models.Model):
    class Meta:
        app_label = 'postgres'
        db_table="tbl_user"
    
    name=models.CharField(max_length=300)
    loginid=models.CharField(max_length=300)
    password=models.CharField(max_length=300)

    def __str__(self):
        return self.name

class UserRole(models.Model):
    class Meta:
        app_label = 'postgres'
        db_table="user_role"
        pass

    loginid=models.CharField(max_length=300)
    role=models.CharField(max_length=300)

    def __str__(self):
        return self.loginid



 
class BusinessObject(models.Model):
    class Meta:
        app_label = 'postgres'
        db_table="business_object"

    name=models.CharField(max_length=300)

    def __str__(self):
        return self.name

class Status(models.Model):
    class Meta:
        app_label = 'postgres'
        db_table="status"
        
    name=models.CharField(max_length=300)
    def __str__(self):
        return self.name

class Action(models.Model):
    class Meta:
        app_label = 'postgres'
        db_table="action"
        
    name=models.CharField(max_length=300)

    def __str__(self):
        return self.name

class Edge(models.Model):
    class Meta:
        app_label = 'postgres'
        db_table="edge"

    objectname=models.CharField(max_length=300)
    nextaction=models.CharField(max_length=300)
    startstatus=models.CharField(max_length=300)
    endstatus=models.CharField(max_length=300)
    role=models.CharField(max_length=300)

    def __str__(self):
        return self.objectname


class CrudEdge(models.Model):
    class Meta:
        app_label = 'postgres'
        db_table="edge_crud"

    objname=models.CharField(max_length=300)
    status=models.CharField(max_length=300)
    role=models.CharField(max_length=300)
    crudaction=models.CharField(max_length=300)

    def __str__(self):
        return self.objname


class BoFieldEditNoAccess(models.Model):
    class Meta:
        app_label = 'postgres'
        db_table="bo_fields_edit_no_access"
    
    user=models.CharField(max_length=300)
    role=models.CharField(max_length=300)
    businessobject=models.CharField(max_length=300)
    attribute=models.CharField(max_length=300)

    def __str__(self):
        return self.user

class BoFieldViewNoAccess(models.Model):
    class Meta:
        app_label = 'postgres'
        db_table="bo_fields_view_no_access"
    
    user=models.CharField(max_length=300)
    role=models.CharField(max_length=300)
    businessobject=models.CharField(max_length=300)
    attribute=models.CharField(max_length=300)

    def __str__(self):
        return self.user

class OutputArray(models.Model):
    dt = models.CharField(max_length=300)
    time = models.CharField(max_length=300)
    input=models.CharField(max_length=300)
    output = models.CharField(max_length=300)
    class Meta:
        abstract = True

class ProcessLog(models.Model):
    class Meta:
        db_table="processlog"
        pass
    dt = models.CharField(max_length=300)
    time = models.CharField(max_length=300)
    processname = models.CharField(max_length=300)
    output = modeldb.ArrayField(
        model_container=OutputArray
    )

class JobRunningStatus(models.Model):
    class Meta:
        db_table="jobrunningstatus"
        pass
    jobid = models.CharField(max_length=300)
    status = models.CharField(max_length=300)

class JobDuration(models.Model):
    class Meta:
        db_table="jobduration"
        pass
    jobid = models.CharField(max_length=300)
    startdatetime = models.CharField(max_length=300)
    enddatetime = models.CharField(max_length=300)
    durationsec = models.CharField(max_length=300)
    jobstatus = models.CharField(max_length=300)

class ProcessHistorical(models.Model):
    class Meta:
        db_table="processhistorical"
        pass
    runid = models.CharField(max_length=300)
    id=models.CharField(max_length=500,primary_key=True)
    jobid = models.CharField(max_length=300)
    dt = models.CharField(max_length=300)
    time = models.CharField(max_length=300)
    processname = models.CharField(max_length=300)
    endtime = models.CharField(max_length=300)
    status = models.CharField(max_length=300)
    output = modeldb.ArrayField(
        model_container=OutputArray
    )

class Process(models.Model):
    class Meta:
        db_table="process"
    name = models.CharField(max_length=200)
    filename = models.CharField(max_length=200)

class Hour(models.Model):
    class Meta:
        app_label = 'postgres'
        db_table = "tbl_hour"

    hr = models.CharField(max_length=300)


class Minute(models.Model):
    class Meta:
        app_label = 'postgres'
        db_table = "tbl_min"

    minute = models.CharField(max_length=300)

class Second(models.Model):
    class Meta:
        app_label = 'postgres'
        db_table = "tbl_second"

    second = models.CharField(max_length=300)


class Month(models.Model):
    class Meta:
        app_label = 'postgres'
        db_table = "tbl_month"

    month = models.CharField(max_length=300)


class Day(models.Model):
    class Meta:
        app_label = 'postgres'
        db_table = "tbl_day"

    day = models.CharField(max_length=300)

class DayOfWeek(models.Model):
    class Meta:
        app_label = 'postgres'
        db_table = "tbl_dayofweek"

    day = models.CharField(max_length=300)

class ProcessSheduler(models.Model):
    class Meta:
        db_table = "processsheduler"
    id = models.CharField(max_length=200,primary_key=True)
    jobname = models.CharField(max_length=200)
    process = models.CharField(max_length=200)
    isweekly = models.CharField(max_length=200)
    ismonthly = models.CharField(max_length=200)
    isdaily = models.CharField(max_length=200)
    ishourly = models.CharField(max_length=200)
    month = models.CharField(max_length=200)
    monthday = models.CharField(max_length=200)         
    weekday = models.CharField(max_length=200)
    hour = models.CharField(max_length=200)
    minute = models.CharField(max_length=200)
    second = models.CharField(max_length=200)
    dayofweek = models.CharField(max_length=200)
    status=models.CharField(max_length=200)


class Prediction(models.Model):
    sn = models.CharField(max_length=300)
    currentrental = models.IntegerField()
    preditime = models.CharField(max_length=300)
    risk = models.CharField(max_length=300)
    npl = models.CharField(max_length=300)
    confi =models.CharField(max_length=300)
    class Meta:
        abstract = True


class BorrowerContract(models.Model):
    contractno = models.CharField(max_length=300)
    loanname = models.CharField(max_length=300)
    loantype = models.CharField(max_length=300)
    principleamount = models.CharField(max_length=300)
    dateofissue = models.CharField(max_length=300)
    interestrate = models.CharField(max_length=300)
    interesttype = models.CharField(max_length=300)
    loanperiod = models.CharField(max_length=300)
    emiamout = models.CharField(max_length=300)
    mortagetype = models.CharField(max_length=300)
    mortageamout = models.CharField(max_length=300)
    noofdaysinarrears = models.CharField(max_length=300)
    loanstatus  = models.CharField(max_length=300)
    contractstatus  = models.CharField(max_length=300)
    class Meta:
        abstract = True

class BorrowerCRM(models.Model):
    ageofrelationmonths=models.CharField(max_length=300)
    totalnoloans=models.CharField(max_length=300)
    totalloanamount=models.CharField(max_length=300)
    class Meta:
        abstract=True

class BorrowerScheduler(models.Model):
    duedate=models.CharField(max_length=300)
    rentalno=models.CharField(max_length=300)
    dueamount=models.CharField(max_length=300)
    duestatus=models.CharField(max_length=300)
    class Meta:
        abstract=True

class BorrowerGeneration(models.Model):
    billdt=models.CharField(max_length=300)
    amount=models.CharField(max_length=300)
    duedate=models.CharField(max_length=300)
    class Meta:
        abstract=True


class BorrowerPayment(models.Model):
    paymentdate=models.CharField(max_length=300)
    paidamount=models.CharField(max_length=300)
    paymentstatus=models.CharField(max_length=300)
    class Meta:
        abstract=True

class Borrower(models.Model):
    class Meta:
        db_table = "borrower"
    borrowername = models.CharField(max_length=300)
    borrowerid = models.CharField(max_length=300)
    img = models.CharField(max_length=300)
    age=models.CharField(max_length=300)
    gender=models.CharField(max_length=300)
    contractno=models.CharField(max_length=300)
    risklevel=models.CharField(max_length=300)
    predidate=models.CharField(max_length=300)
    district=models.CharField(max_length=300)
    dob=models.CharField(max_length=300)
    maritalstatus=models.CharField(max_length=300)
    addresslinethree=models.CharField(max_length=300)
    province=models.CharField(max_length=300)
    mobileno=models.CharField(max_length=300)
    predata = modeldb.ArrayField(
        model_container=Prediction
    )
    
    contract = modeldb.ArrayField(
        model_container=BorrowerContract
    )

    crm = modeldb.ArrayField(
        model_container=BorrowerCRM
    )
    scheduler = modeldb.ArrayField(
        model_container=BorrowerScheduler
    )

    billgeneration = modeldb.ArrayField(
        model_container=BorrowerGeneration
    )
    paymentbill=modeldb.ArrayField(
        model_container=BorrowerPayment
    )
   
    def __str__(self):
        return self.name