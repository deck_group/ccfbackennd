from django.urls import path
from .views import *

urlpatterns = [
     path(r'test',test),
     path(r'borrowers',BorrowerApi.as_view(),name='borrower-create'),
     path(r'borrowers/<int:id>', BorrowerDetailApi.as_view()),


     path(r'logauths',Authentication.as_view()),
     path(r'processlogs',ProcessRunning.as_view()),
     path(r'nextallowableactions',NextAllowableActions.as_view(),name='edge-create'),
     path(r'nextcrudactions',NextAllowableCrudAction.as_view(),name='edgecrud-create'),
     path(r'bofieldseditnoaccesses',GetNotEditableFields.as_view(),name='bofieldseditnoaccesses-create'),
     path(r'bofieldsviewnoaccesses',GetNotViewableFields.as_view(),name='bofieldsviewnoaccesses-create'),
     path(r'processes',ProcessApi.as_view(),name='hour-create'),
     path(r'hours',HourApi.as_view(),name='hour-create'),
     path(r'minutes',MinuteApi.as_view(),name='minute-create'),
     path(r'seconds',SecondApi.as_view(),name='minute-create'),
     path(r'dayofweeks',DayofWeekApi.as_view(),name='minute-create'),
     path(r'days',DayApi.as_view(),name='minute-create'),
     path(r'processshedulers',ProcesssSchedulerApi.as_view(),name='loanapplication-create'),
     path(r'processshedulers/<int:id>', ProcesssSchedulerDetailApi.as_view()),   
     path(r'jobstatuses', JobRunningStatusApi.as_view()),   
     path(r'jobstatuses/<int:jobid>', JobRunningStatusDetailApi.as_view()),   
     path(r'joblogs',ProcessLogHistoricalApi.as_view()),   
     path(r'joblogs/<str:id>',ProcessLogHistoricalDetailApi.as_view()),   

]