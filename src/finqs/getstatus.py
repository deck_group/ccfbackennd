import redis
from time import sleep
from threading import Thread
import json
from .models import JobRunningStatus
from datetime import datetime
from pytz import timezone 
from .externalurls import *


redis_r = redis.Redis(host=exredishost, port=6379, db=0,password=expass)
redis_p = redis_r.pubsub()
redis_p.subscribe("getstatusdbupdate")
st_log = redis.Redis(host=exredishost, port=6379, db=0,password=expass)


class StatusPub(Thread):
    def __init__(self):
        Thread.__init__(self)
        self.daemon = True
        self.start()
        pass
    def run(self):

        while True:
            message=redis_p.get_message()
            encoding = 'utf-8'
            if(message):
                if(message['data'] != 1):
                    print(message['data'])
                    dic=str(message['data'],encoding)
                    res = json.loads(dic)
                    for key in res["data"]:
                        JobRunningStatus.objects.filter(jobid=key).update(status=res["data"][key])
                        pass
                    dataJOB=JobRunningStatus.objects.all()
                    dict={}
                    for data in dataJOB:
                        dict[data.jobid]=data.status
                        pass
                    st_log.publish('statusdbupdated',json.dumps(dict))
                    pass
                pass
            pass
        pass
    pass

StatusPub()
