from django.apps import AppConfig


class FinqsConfig(AppConfig):
    name = 'finqs'
